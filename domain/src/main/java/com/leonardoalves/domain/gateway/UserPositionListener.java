package com.leonardoalves.domain.gateway;

/**
 * Created by leona on 21/05/2017.
 */

public interface UserPositionListener {
    void userCurrentPosition(double lon, double lat);
    void locationNotAcquired();
}
