package com.leonardoalves.domain.interactor;

import com.leonardoalves.data.enums.Units;
import com.leonardoalves.data.repository.api.get.CitiesTemperature;
import com.leonardoalves.domain.interfaces.ForecastListListener;

/**
 * Created by leona on 21/05/2017.
 */

public class Cities {

    public void getCities(final double lat, final double lon, Units unity, final ForecastListListener listener){
        CitiesTemperature citiesTemperature = new CitiesTemperature();
        citiesTemperature.getCitiesAround(lat, lon, unity, new ForecastInRange(lat, lon, listener));
    }
}
