package com.leonardoalves.domain.interactor;

import android.location.Location;

import com.leonardoalves.data.entities.Forecast;
import com.leonardoalves.data.entities.ForecastInBound;
import com.leonardoalves.data.entities.ForecastResult;
import com.leonardoalves.data.repository.api.get.CitiesTemperatureListener;
import com.leonardoalves.domain.interfaces.ForecastListListener;

import java.util.ArrayList;

/**
 * Created by leona on 21/05/2017.
 */

public class ForecastInRange implements CitiesTemperatureListener {

    public static final int MAX_DISTANCE = 50000; //50 KM
    final double lat;
    final double lon;
    private final ForecastListListener listener;

    public ForecastInRange(double lat, double lon, ForecastListListener listener) {
        this.lat = lat;
        this.lon = lon;
        this.listener = listener;
    }

    @Override
    public void getCitiesInRange(ForecastResult result) {
        if (result == null){
            listener.getCitiesInCycle(new ArrayList<Forecast>());
            return;
        }
        Location center = new Location("center");
        center.setLatitude(lat);
        center.setLongitude(lon);
        ArrayList<Forecast> forecasts = new ArrayList<>();
        for (Forecast forecast : result.getForecast()) {
            Location city = new Location("city");
            city.setLatitude(forecast.getCoord().getLat());
            city.setLongitude(forecast.getCoord().getLon());
            float distance = center.distanceTo(city);
            forecast.getCoord().setDistance(distance);
            if (distance < MAX_DISTANCE){
                forecasts.add(forecast);
            }
        }
        listener.getCitiesInCycle(forecasts);
    }
}
