package com.leonardoalves.domain.interfaces;

import com.leonardoalves.data.entities.Forecast;
import com.leonardoalves.data.entities.ForecastInBound;
import com.leonardoalves.data.entities.ForecastResult;

import java.util.List;

/**
 * Created by leona on 21/05/2017.
 */

public interface ForecastListListener {
    void getCitiesInCycle(List<Forecast> cities);
}
