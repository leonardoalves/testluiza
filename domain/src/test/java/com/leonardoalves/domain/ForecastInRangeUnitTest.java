package com.leonardoalves.domain;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * ForecastInRange local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ForecastInRangeUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}