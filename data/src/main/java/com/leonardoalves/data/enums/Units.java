package com.leonardoalves.data.enums;

/**
 * Created by leona on 21/05/2017.
 */

public enum Units {
    IMPERIAL("imperial"),
    METRIC("metric");

    private final String text;

    private Units(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
