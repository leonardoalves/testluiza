package com.leonardoalves.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by leona on 21/05/2017.
 */

public class ForecastInBound {
    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("cnt")
    @Expose
    private Integer count;
    @SerializedName("list")
    @Expose
    private java.util.List<Forecast> forecast = null;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public java.util.List<Forecast> getForecast() {
        return forecast;
    }

    public void setForecast(java.util.List<Forecast> forecast) {
        this.forecast = forecast;
    }
}
