package com.leonardoalves.data.repository.api;

import com.leonardoalves.data.entities.ForecastInBound;
import com.leonardoalves.data.entities.ForecastResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by leona on 21/05/2017.
 */

public interface ApiInterface {
    @GET("find?cnt=50&APPID=115e35f1ef5b9e3c88a9a8e627f6a214&lang=pt")
    Call<ForecastResult> getArround(@Query("lat") double lat, @Query("lon") double lon, @Query("units") String units);
    @GET("box/city?APPID=115e35f1ef5b9e3c88a9a8e627f6a214")
    Call<ForecastInBound> getBox(@Query("bbox") String bbox);
}
