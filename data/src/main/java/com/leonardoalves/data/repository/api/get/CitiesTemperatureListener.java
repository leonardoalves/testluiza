package com.leonardoalves.data.repository.api.get;

import com.leonardoalves.data.entities.ForecastInBound;
import com.leonardoalves.data.entities.ForecastResult;

/**
 * Created by leona on 21/05/2017.
 */

public interface CitiesTemperatureListener {
    /**
     * Filters results within an area
     * @param cities
     */
    void getCitiesInRange(ForecastResult cities);
}
