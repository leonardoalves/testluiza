package com.leonardoalves.data.repository.api.get;

import android.util.Log;

import com.leonardoalves.data.entities.ForecastInBound;
import com.leonardoalves.data.entities.ForecastResult;
import com.leonardoalves.data.enums.Units;
import com.leonardoalves.data.repository.api.ApiClient;
import com.leonardoalves.data.repository.api.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by leona on 21/05/2017.
 */

public class CitiesTemperature {
    private static final String TAG = "GET_DATA_FROM_SERVER";

    public void getCitiesAround(double lat, double lon, Units unity, final CitiesTemperatureListener listener){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ForecastResult> call = apiService.getArround(lat, lon, unity.toString());
        call.enqueue(new Callback<ForecastResult>() {
            @Override
            public void onResponse(Call<ForecastResult>call, Response<ForecastResult> response) {
                ForecastResult forecast = response.body();
                listener.getCitiesInRange(forecast);
            }

            @Override
            public void onFailure(Call<ForecastResult>call, Throwable t) {
                //TODO: Error message
                Log.e(TAG, t.toString());
            }
        });
    }

    public void getInBox(double right, double top, double bot, double left, Units unity, final CitiesTemperatureListener listener){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        String bbox = left +","+bot+","+right+","+left;
        Call<ForecastInBound> call = apiService.getBox(bbox);
        call.enqueue(new Callback<ForecastInBound>() {
            @Override
            public void onResponse(Call<ForecastInBound>call, Response<ForecastInBound> response) {
                ForecastInBound forecast = response.body();
//                listener.getCitiesInRange(forecast);
            }

            @Override
            public void onFailure(Call<ForecastInBound>call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }
}
