package com.leonardoalves.desafioluizalabs.presenter;

import android.app.Activity;

import com.leonardoalves.data.entities.Forecast;
import com.leonardoalves.data.enums.Units;
import com.leonardoalves.desafioluizalabs.viewData.ForecastData;
import com.leonardoalves.domain.gateway.UserPositionListener;
import com.leonardoalves.domain.interactor.Cities;
import com.leonardoalves.domain.interactor.UserPosition;
import com.leonardoalves.domain.interfaces.ForecastListListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by leona on 21/05/2017.
 */

public class ForecastListPresenter {
    Activity context;

    public ForecastListPresenter(Activity context) {
        this.context = context;
    }

    public void getListOfCitiesArround(final Units unit, final com.leonardoalves.desafioluizalabs.interfaces.ForecastListListener forecastListListener){
        final UserPosition userPosition = new UserPosition(context);
        userPosition.aquireUserPosition(new UserPositionListener() {
            @Override
            public void userCurrentPosition(double lon, double lat) {
                Cities cities = new Cities();
                cities.getCities(lat, lon, unit, new ForecastListListener() {
                    @Override
                    public void getCitiesInCycle(List<Forecast> cities) {
                        userPosition.stopListening();
                        if (cities == null){
                            return;
                        }
                        Collections.sort(cities, new Comparator<Forecast>() {
                            @Override
                            public int compare(Forecast o1, Forecast o2) {
                                return Float.valueOf(o1.getCoord().getDistance()).compareTo(Float.valueOf(o2.getCoord().getDistance()));
                            }
                        });
                        ArrayList<ForecastData> forecastDatas = extractForecastInformation(cities, unit);
                        forecastListListener.adForecastInformation(forecastDatas);
                    }
                });
            }

            @Override
            public void locationNotAcquired() {
                System.out.println("Sem posicao");
                forecastListListener.locationFailed();
            }
        });

    }

    private ArrayList<ForecastData> extractForecastInformation(List<Forecast> forecasts, Units unit) {
        ArrayList<ForecastData> forecastList = new ArrayList<>();
        for (Forecast forecast : forecasts) {
            ForecastData forecastData = new ForecastData();
            forecastData.setCityName(forecast.getName());
            forecastData.setIcon("http://openweathermap.org/img/w/" + forecast.getWeather().get(0).getIcon()+".png");
            String temperature = "";
            temperature = String.valueOf(forecast.getMain().getTempMax().intValue());
            forecastData.setMaxTemperature(temperature + "º");
            temperature = String.valueOf(forecast.getMain().getTempMin().intValue());
            forecastData.setMinTemperature(temperature + "º");
            temperature = String.valueOf(forecast.getMain().getTemp().intValue());
            forecastData.setTemperature( temperature + "º");
            forecastData.setWeather(forecast.getWeather().get(0).getMain());
            forecastData.setHumidity(String.valueOf(forecast.getMain().getHumidity().intValue()) + "%");
            String speedUnit = (unit.equals(Units.METRIC)?"Km/h":"Mph");
            forecastData.setWind(String.valueOf(forecast.getWind().getSpeed() +speedUnit+ " " + forecast.getWind().getDeg()+"º"));
            forecastData.setDistance(distanceToText(forecast.getCoord().getDistance(), unit));
            forecastList.add(forecastData);
        }
        return forecastList;
    }

    private String distanceToText(Float distance, Units unit){
        Long dist = Long.valueOf(distance.intValue());
        if (dist <= 1000L){
            if (unit.equals(Units.IMPERIAL)){
                Float footDistance = dist/3.2808F;
                dist = Long.valueOf(footDistance.intValue());
            }
            return String.valueOf(dist + (unit.equals(Units.METRIC)?" m":" ft"));
        }
        if (unit.equals(Units.IMPERIAL)){
            distance = dist/1.609344F;
        }
        BigDecimal bigDistance = new BigDecimal(distance/1000).setScale(2, BigDecimal.ROUND_FLOOR);
        return (bigDistance.toString() + (unit.equals(Units.METRIC)?" Km":" Miles"));
    }
}
