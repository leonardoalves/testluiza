package com.leonardoalves.desafioluizalabs.presenter;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.leonardoalves.data.entities.Forecast;
import com.leonardoalves.data.entities.ForecastInBound;
import com.leonardoalves.data.entities.ForecastResult;
import com.leonardoalves.data.enums.Units;
import com.leonardoalves.desafioluizalabs.interfaces.MarkerListListener;
import com.leonardoalves.domain.interactor.Cities;
import com.leonardoalves.domain.interfaces.ForecastListListener;

import java.util.List;

/**
 * Created by leonardo on 22/05/17.
 */

public class ForecastMapPresenter {
    public static void getMapMarkets(LatLng position, final Units units, final MarkerListListener listener){
        Cities cities = new Cities();
        cities.getCities(position.latitude, position.longitude, units, new ForecastListListener() {
            @Override
            public void getCitiesInCycle(List<Forecast> cities) {
                for (Forecast forecast : cities) {
                    LatLng latLng = new LatLng(forecast.getCoord().getLat(), forecast.getCoord().getLon());
                    MarkerOptions marker = new MarkerOptions().position(latLng);
                    Integer temperature = forecast.getMain().getTemp().intValue();
                    marker.title(temperature.toString() + (units.equals(Units.METRIC)?" ºC":" ºF"));
                    marker.snippet("http://openweathermap.org/img/w/" + forecast.getWeather().get(0).getIcon()+".png");
                    listener.addMarker(marker);
                }
            }
        });
    }
}
