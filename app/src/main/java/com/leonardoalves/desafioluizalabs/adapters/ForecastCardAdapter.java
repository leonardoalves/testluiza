package com.leonardoalves.desafioluizalabs.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.leonardoalves.desafioluizalabs.viewData.ForecastData;
import com.leonardoalves.desafioluizalabs.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leona on 21/05/2017.
 */

public class ForecastCardAdapter extends RecyclerView.Adapter<ForecastCardAdapter.ViewHolder> {

    ArrayList<ForecastData> dataSet;

    public ForecastCardAdapter(ArrayList<ForecastData> dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.weather_card, parent, false);
        return new ViewHolder(inflatedView);    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ForecastData forecastData = dataSet.get(position);
        holder.cityName.setText(forecastData.getCityName());
        holder.weather.setText(forecastData.getWeather());
        holder.temperature.setText(forecastData.getTemperature());
        holder.minTemperature.setText(forecastData.getMinTemperature());
        holder.maxTemperature.setText(forecastData.getMaxTemperature());
        holder.humidity.setText(forecastData.getHumidity());
        holder.wind.setText(forecastData.getWind());
        holder.distance.setText(forecastData.getDistance());
        Ion.with(holder.iconWeather)
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.error_image)
                .animateIn(R.anim.fade_in)
                .load(forecastData.getIcon());

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cityName)
        public TextView cityName;
        @BindView(R.id.weather)
        public TextView weather;
        @BindView(R.id.temperature)
        public TextView temperature;
        @BindView(R.id.minTemperature)
        public TextView minTemperature;
        @BindView(R.id.maxTemperature)
        public TextView maxTemperature;
        @BindView(R.id.wind)
        public TextView wind;
        @BindView(R.id.humidity)
        public TextView humidity;
        @BindView(R.id.iconWeather)
        public ImageView iconWeather;
        @BindView(R.id.distance)
        public TextView distance;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
