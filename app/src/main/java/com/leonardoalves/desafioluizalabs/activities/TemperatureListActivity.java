package com.leonardoalves.desafioluizalabs.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.leonardoalves.data.enums.Units;
import com.leonardoalves.desafioluizalabs.EmptyRecyclerView;
import com.leonardoalves.desafioluizalabs.adapters.ForecastCardAdapter;
import com.leonardoalves.desafioluizalabs.viewData.ForecastData;
import com.leonardoalves.desafioluizalabs.interfaces.ForecastListListener;
import com.leonardoalves.desafioluizalabs.R;
import com.leonardoalves.desafioluizalabs.presenter.ForecastListPresenter;
import com.leonardoalves.domain.interactor.UserPosition;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TemperatureListActivity extends AppCompatActivity {
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.forecast_list)
    EmptyRecyclerView forecastList;
    @BindView(R.id.location_locked)
    ConstraintLayout location_locked;
    @BindView(R.id.location_failed)
    LinearLayout location_failed;
    @BindView(R.id.empty_list)
    LinearLayout empty_list;

    private ForecastListPresenter forecastListPresenter;
    private RecyclerView.LayoutManager layoutManager;
    private ForecastCardAdapter adapter;
    private ArrayList<ForecastData> forecastDatas;
    private Units unit = Units.METRIC;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_list);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        ButterKnife.bind(this);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                forecastListPresenter.getListOfCitiesArround(unit, new ForecastListListener() {
                    @Override
                    public void adForecastInformation(ArrayList<ForecastData> forecastData) {
                        clearAndUpdateList(forecastData);
                    }

                    @Override
                    public void locationFailed() {
                        empty_list.setVisibility(View.GONE);
                        forecastList.setVisibility(View.GONE);
                        location_locked.setVisibility(View.GONE);
                        location_failed.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void internetProblem() {
                        empty_list.setVisibility(View.VISIBLE);
                        forecastList.setVisibility(View.GONE);
                        location_locked.setVisibility(View.GONE);
                        location_failed.setVisibility(View.GONE);
                    }
                });
            }
        });


        layoutManager = new LinearLayoutManager(this);
        forecastList.setLayoutManager(layoutManager);
        forecastList.setEmptyView(empty_list);
        forecastDatas = new ArrayList<>();
        adapter = new ForecastCardAdapter(forecastDatas);
        forecastList.setAdapter(adapter);

        forecastListPresenter = new ForecastListPresenter(this);
        swipeRefreshLayout.setRefreshing(true);
        forecastListPresenter.getListOfCitiesArround(unit, new ForecastListListener() {
            @Override
            public void adForecastInformation(ArrayList<ForecastData> forecastData) {
                insertItens(forecastData);
            }
            @Override
            public void locationFailed() {
                empty_list.setVisibility(View.GONE);
                forecastList.setVisibility(View.GONE);
                location_locked.setVisibility(View.GONE);
                location_failed.setVisibility(View.VISIBLE);
            }

            @Override
            public void internetProblem() {
                empty_list.setVisibility(View.VISIBLE);
                forecastList.setVisibility(View.GONE);
                location_locked.setVisibility(View.GONE);
                location_failed.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.forecast_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_map:
                Intent intent = new Intent(this, MapsActivity.class);
                intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                break;
            case R.id.action_measure:
                swipeRefreshLayout.setRefreshing(true);
                if (unit.equals(Units.METRIC)){
                    unit = Units.IMPERIAL;
                    item.setIcon(R.drawable.celcius_icon);
                }else{
                    unit = Units.METRIC;
                    item.setIcon(R.drawable.fahrenheit_icon);
                }
                forecastListPresenter.getListOfCitiesArround(unit, new ForecastListListener() {
                    @Override
                    public void adForecastInformation(ArrayList<ForecastData> forecastData) {
                        clearAndUpdateList(forecastData);
                    }
                    @Override
                    public void locationFailed() {
                        empty_list.setVisibility(View.GONE);
                        forecastList.setVisibility(View.GONE);
                        location_locked.setVisibility(View.GONE);
                        location_failed.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void internetProblem() {
                        empty_list.setVisibility(View.VISIBLE);
                        forecastList.setVisibility(View.GONE);
                        location_locked.setVisibility(View.GONE);
                        location_failed.setVisibility(View.GONE);
                    }
                });
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case UserPosition.PERMISSION_ACCESS_COARSE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    swipeRefreshLayout.setRefreshing(true);
                    forecastListPresenter.getListOfCitiesArround(unit, new ForecastListListener() {
                        @Override
                        public void adForecastInformation(ArrayList<ForecastData> forecastData) {
                            forecastList.setVisibility(View.VISIBLE);
                            location_locked.setVisibility(View.GONE);
                            insertItens(forecastData);
                        }
                        @Override
                        public void locationFailed() {
                            empty_list.setVisibility(View.GONE);
                            forecastList.setVisibility(View.GONE);
                            location_locked.setVisibility(View.GONE);
                            location_failed.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void internetProblem() {
                            empty_list.setVisibility(View.VISIBLE);
                            forecastList.setVisibility(View.GONE);
                            location_locked.setVisibility(View.GONE);
                            location_failed.setVisibility(View.GONE);
                        }
                    });
                } else {
                    forecastList.setVisibility(View.GONE);
                    location_locked.setVisibility(View.VISIBLE);
                }
                return;
            }
            case UserPosition.PERMISSION_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    swipeRefreshLayout.setRefreshing(true);
                    forecastListPresenter.getListOfCitiesArround(unit, new ForecastListListener() {
                        @Override
                        public void adForecastInformation(ArrayList<ForecastData> forecastData) {
                            forecastList.setVisibility(View.VISIBLE);
                            location_locked.setVisibility(View.GONE);
                            insertItens(forecastData);
                        }
                        @Override
                        public void locationFailed() {
                            empty_list.setVisibility(View.GONE);
                            forecastList.setVisibility(View.GONE);
                            location_locked.setVisibility(View.GONE);
                            location_failed.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void internetProblem() {
                            empty_list.setVisibility(View.VISIBLE);
                            forecastList.setVisibility(View.GONE);
                            location_locked.setVisibility(View.GONE);
                            location_failed.setVisibility(View.GONE);
                        }
                    });
                } else {
                    forecastList.setVisibility(View.GONE);
                    location_locked.setVisibility(View.VISIBLE);
                }
                return;
        }
    }

    private void insertItens(ArrayList<ForecastData> forecastData) {
        swipeRefreshLayout.setRefreshing(false);
        empty_list.setVisibility(View.GONE);
        forecastList.setVisibility(View.VISIBLE);
        location_locked.setVisibility(View.GONE);
        location_failed.setVisibility(View.GONE);
        int size = forecastDatas.size();
        forecastDatas.addAll(forecastData);
        adapter.notifyItemInserted(size);
    }

    private void clearAndUpdateList(ArrayList<ForecastData> forecastData) {
        swipeRefreshLayout.setRefreshing(false);
        empty_list.setVisibility(View.GONE);
        forecastList.setVisibility(View.VISIBLE);
        location_locked.setVisibility(View.GONE);
        location_failed.setVisibility(View.GONE);
        int size = forecastDatas.size();
        forecastDatas.clear();
        forecastList.removeAllViews();
        adapter.notifyItemRangeRemoved(0, size);
        forecastDatas.addAll(forecastData);
        adapter.notifyItemInserted(size);
    }

}
