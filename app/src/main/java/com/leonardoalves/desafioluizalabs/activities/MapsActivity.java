package com.leonardoalves.desafioluizalabs.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.koushikdutta.ion.Ion;
import com.leonardoalves.data.enums.Units;
import com.leonardoalves.desafioluizalabs.R;
import com.leonardoalves.desafioluizalabs.interfaces.MarkerListListener;
import com.leonardoalves.desafioluizalabs.presenter.ForecastMapPresenter;
import com.leonardoalves.domain.gateway.UserPositionListener;
import com.leonardoalves.domain.interactor.UserPosition;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter {

    private GoogleMap mMap;
    Marker lastOpenned = null;
    private Units unit = Units.METRIC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.leonardoalves.desafioluizalabs.R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        UserPosition userPosition = new UserPosition(this);
        userPosition.aquireUserPosition(new UserPositionListener() {
            @Override
            public void userCurrentPosition(double lon, double lat) {
                LatLng userPosition = new LatLng(lat, lon);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(userPosition));
            }

            @Override
            public void locationNotAcquired() {

            }
        });
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                mMap.clear();
            }
        });
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng position = mMap.getCameraPosition().target;
                Circle area = mMap.addCircle(new CircleOptions().center(position).radius(50000).visible(false));//.visible(true).fillColor(R.color.cardview_shadow_start_color));
                mMap.setMaxZoomPreference(getZoomLevel(area));
                mMap.setMinZoomPreference(getZoomLevel(area));
                ForecastMapPresenter.getMapMarkets(position, unit, new MarkerListListener() {
                    @Override
                    public void addMarker(MarkerOptions marker) {
                        mMap.addMarker(marker);
                    }
                });
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                if (lastOpenned != null) {
                    lastOpenned.hideInfoWindow();
                    if (lastOpenned.equals(marker)) {
                        lastOpenned = null;
                        return true;
                    }
                }
                marker.showInfoWindow();
                lastOpenned = marker;
                return true;
            }
        });
        mMap.setInfoWindowAdapter(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_map:
                Intent intent = new Intent(this, TemperatureListActivity.class);
                intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                break;
            case R.id.action_measure:
                Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT)
                        .show();
                if (unit.equals(Units.METRIC)){
                    unit = Units.IMPERIAL;
                    item.setIcon(R.drawable.celcius_icon);
                }else{
                    unit = Units.METRIC;
                    item.setIcon(R.drawable.fahrenheit_icon);
                }
                LatLng position = mMap.getCameraPosition().target;
                mMap.clear();
                ForecastMapPresenter.getMapMarkets(position, unit, new MarkerListListener() {
                    @Override
                    public void addMarker(MarkerOptions marker) {
                        mMap.addMarker(marker);
                    }
                });
                break;
            default:
                break;
        }

        return true;
    }


    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return prepareInfoView(marker);
    }

    private View prepareInfoView(Marker marker) {
        LinearLayout infoView = new LinearLayout(MapsActivity.this);
        getLayoutInflater().inflate(R.layout.forecast_infoview, infoView);
        ImageView weatherIcon = (ImageView) infoView.findViewById(R.id.iconWeather);
        Ion.with(this)
                .load(marker.getSnippet())
                .withBitmap()
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.error_image)
                .animateIn(R.anim.fade_in)
                .intoImageView(weatherIcon);
        TextView temperature = (TextView) infoView.findViewById(R.id.temperature);
        temperature.setText(marker.getTitle());
        return infoView;
    }
    public float getZoomLevel(Circle circle) {
        float zoomLevel = 11;
        if (circle != null) {
            double radius = circle.getRadius() + circle.getRadius() / 2;
            double scale = radius / 500;
            zoomLevel = (float) (16 - Math.log(scale) / Math.log(2));
        }
        return zoomLevel;
    }
}
