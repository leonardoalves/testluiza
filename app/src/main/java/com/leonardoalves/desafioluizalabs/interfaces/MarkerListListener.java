package com.leonardoalves.desafioluizalabs.interfaces;

import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by leonardo on 22/05/17.
 */

public interface MarkerListListener {
    void addMarker(MarkerOptions marker);
}
