package com.leonardoalves.desafioluizalabs.interfaces;

import com.leonardoalves.desafioluizalabs.viewData.ForecastData;

import java.util.ArrayList;

/**
 * Created by leona on 21/05/2017.
 */

public interface ForecastListListener {
    void adForecastInformation(ArrayList<ForecastData> forecastData);
    void locationFailed();
    void internetProblem();
}
